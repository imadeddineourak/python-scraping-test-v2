import gzip
import json

def read_categories_from_json(filepath):
    with gzip.open(filepath, 'rt', encoding='utf-8') as file:
        categories_data = json.load(file)
    return categories_data

def parse_categories():
    filepath = 'third_part/categories.json.gz'
    categories_data = read_categories_from_json(filepath)
    
    def get_categories_recursive(categories):
        result = []
        for category in categories:
            result.append({
                'id': category['id'],
                'name': category['name'],
                'subcategories': get_categories_recursive(category.get('subcategories', []))
            })
        return result
    
    categories = get_categories_recursive(categories_data)
    return categories
