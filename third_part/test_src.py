import json

from third_part.src import parse_categories


def test_parse_categories():
    categories = parse_categories()
    with open('./third_part/expected_categories.json') as f:
        expected_categories = json.load(f)
    assert categories == expected_categories

