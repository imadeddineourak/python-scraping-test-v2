import scrapy
from scrapy.loader import ItemLoader
from scrapy.loader.processors import TakeFirst, Join

class ProductItem(scrapy.Item):
    name = scrapy.Field(output_processor=TakeFirst())
    breadcrumb = scrapy.Field(output_processor=Join(separator=' > '))

class WoolworthsSpider(scrapy.Spider):
    name = "woolworths"
    start_urls = [
        'https://www.woolworths.com.au/shop/browse/drinks/cordials-juices-iced-teas/iced-teas'
    ]

    def parse(self, response):
        breadcrumb = ["Home", "Drinks", "Cordials, Juices & Iced Teas", "Iced Teas"]
        
        products = response.css('.shelfProductTile-descriptionLink::text').getall()

        for product in products:
            loader = ItemLoader(item=ProductItem())
            loader.add_value('name', product)
            loader.add_value('breadcrumb', breadcrumb)
            yield loader.load_item()
