from first_part.src import find_max_occurrence,parse_nutritional_values,is_grandma_list



#////////////////// Test TASK 01

keywords = ['milk', 'chocolate', 'c+', 'python', 'cat', 'dog']

# Appeler la fonction pour trouver les séquences avec la valeur maximale
max_words = find_max_occurrence(keywords)

# Afficher les résultats
print("Séquences ayant la valeur maximale dans les mots-clés :", max_words)




#////////////////// Test TASK 02

from first_part.src import parse_nutritional_values

text = """
Additifs nutritionnels : Vitamine C-D3 : 160 UI, Fer (3b103) : 4 mg, Iode (3b202) : 0,28 mg, Cuivre (3b405, 3b406) : 2,2 mg, Manganèse (3b502, 3b503, 3b504) : 1,1 mg, Zinc (3b603, 3b605, 3b606) : 11 mg –Clinoptilolited’origine sédimentaire : 2 g. Protéine : 11,0 % - Teneur en matières grasses : 4,5 % - Cendres brutes : 1,7 % - Cellulose brute : 0,5 % - Humidité : 80,0 %.
"""

result = parse_nutritional_values(text)
print(result)




#////////////////// Test TASK 03

from first_part.src import is_grandma_list

example1 = [1, 2, [[4, 5], [4, 7]], 5, 4, [[95], [2]]]
example2 = [5, 9, 4, [[8, 7]], 4, 7, [[5]]]

# Test the is_grandma_list function with the examples
result1 = is_grandma_list(example1)
result2 = is_grandma_list(example2)

# Print the results
print(f"Example 1 is grandma list: {result1}")  
print(f"Example 2 is grandma list: {result2}")
