#//////////////////////   TASK01
def find_max_occurrence(keywords):
    # liste pour stocker les séquences de paires de caractères
    sequences = []

    # Parcourir chaque mot-clé dans la liste keywords
    for s in keywords:
        # Parcourir les indices de 0 à len(s)-2 dans chaque mot-clé
        for i in range(len(s) - 1):
            # Ajouter la paire de caractères consécutifs à la liste sequences
            sequences.append(s[i] + s[i+1])

    occurrences = {}
    for seq in sequences:
        if seq in occurrences:
            occurrences[seq] += 1
        else:
            occurrences[seq] = 1

    # Trouver l'occurrence maximale de n'importe quelle séquence
    max_occurrence = max(occurrences.values())

    max_words = []

    # Parcourir le dictionnaire et collecter les séquences ayant la valeur maximale
    for word, value in occurrences.items():
        if value == max_occurrence:
            max_words.append(word)

    # Retourner les séquences ayant la valeur maximale
    return max_words


#//////////////////////   TASK02
import re

def parse_nutritional_values(text):
    nutrition_dict = {}

    # Replace any special hyphens with commas and normalize whitespace
    text = re.sub(r'[–]', ',', text)
    text = re.sub(r'\s+', ' ', text).strip()

    # Start parsing from "Vitamine C-D3" if present
    start_index = text.find("Vitamine C-D3")
    if start_index != -1:
        text = text[start_index:]

    # Refined pattern to match nutritional information
    pattern = r'([a-zA-Zéèàêâôîûç\s\(\)0-9,-]+)\s*:\s*([\d,.]+ ?[a-zA-Z%UI]*)'

    # Split text based on commas and handle units properly
    segments = re.split(r',\s*(?!\d)', text)

    for segment in segments:
        matches = re.findall(pattern, segment)
        for match in matches:
            name, value = match
            name = name.strip()
            value = value.strip()
            if name and value:
                nutrition_dict[name] = value

    return nutrition_dict





#//////////////////////   TASK03
def flatten(lst):
    """Flatten a nested list into a single list."""
    flat_list = []
    for item in lst:
        if isinstance(item, list):
            flat_list.extend(flatten(item))
        else:
            flat_list.append(item)
    return flat_list

def check_products(sublist, flat_list):
    """Check if any product of adjacent numbers in a sublist exists in the flattened list."""
    for i in range(len(sublist)):
        for j in range(i + 1, len(sublist)):
            if isinstance(sublist[i], int) and isinstance(sublist[j], int):
                product = 1
                for k in range(i, j + 1):
                    product *= sublist[k]
                if product in flat_list:
                    return True
    return False

def is_grandma_list(lst):
    """Determine if a list is a 'grandma list'."""
    flat_list = flatten(lst)
    queue = [lst]

    while queue:
        current = queue.pop(0)
        if isinstance(current, list):
            if check_products(current, flat_list):
                return True
            for item in current:
                queue.append(item)
    
    return False

# Example lists to test the function
list1 = [1, 2, [[4, 5], [4, 7]], 5, 4, [[95], [2]]]
list2 = [5, 9, 4, [[8, 7]], 4, 7, [[5]]]

print(is_grandma_list(list1))  # Expected output: True
print(is_grandma_list(list2))  # Expected output: False
