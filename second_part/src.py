
# task 1
import requests

def http_request():
    url = 'https://europe-west1-dataimpact-preproduction.cloudfunctions.net/recruitement_test_requests?task=1'

    # Effectuer la requête GET
    response = requests.get(url)

    # Vérifier si la requête a réussi (code 200) et si la réponse est en JSON
    if response.status_code == 200:
        try:
            json_response = response.json()
            if 'success' in json_response:
                return json_response['success']
            else:
                return None  
        except ValueError:
            return None  
    else:
        return None  

#task 2


# task 3
from datetime import date, timedelta

# Décorateur pour formater la date comme 'yyyy-mm/d'
def format_date(func):
    def wrapper(*args, **kwargs):
        result = func(*args, **kwargs)
        formatted_date = result.strftime('%Y-%m/%d')
        return formatted_date
    return wrapper

# Fonction pour obtenir le premier jour de la dernière semaine du mois en cours
@format_date
def first_day_of_last_week_in_current_month():
    today = date.today()
    # Trouver le dernier jour du mois en cours
    last_day_of_month = date(today.year, today.month % 12 + 1, 1) - timedelta(days=1)
    # Calculer le premier jour de la dernière semaine
    first_day_of_last_week = last_day_of_month - timedelta(days=last_day_of_month.weekday() + 6)
    return first_day_of_last_week

# task 4
class CacheDecorator:
    """Saves the results of a function according to its parameters"""
    def __init__(self):
        self.cache = {}

    def __call__(self, func):
        def _wrap(*a, **kw):
            if a[0] not in self.cache:
                self.cache[a[0]] = func(*a, **kw)
            return self.cache[a[0]]

        return _wrap

# task 5


class LoginMetaClass(type):
    def __new__(cls, name, bases, dct):
        # Intercept method calls to enforce login check
        for method_name, method in dct.items():
            if callable(method) and method_name != '__init__':
                dct[method_name] = cls.wrap_method(method)
        return super().__new__(cls, name, bases, dct)

    @staticmethod
    def wrap_method(method):
        def wrapped(self, *args, **kwargs):
            if getattr(self, 'logged_in', False):
                return method(self, *args, **kwargs)
            else:
                raise Exception(f"Cannot call '{method.__name__}' method: Not logged in")
        return wrapped

class AccessWebsite(metaclass=LoginMetaClass):
    logged_in = False

    def login(self, username, password):
        if username == "admin" and password == "admin":
            self.logged_in = True

    def access_website(self):
        return "Success"
