import pytest
from second_part.src import AccessWebsite, first_day_of_last_week_in_current_month, http_request

#test task 1
success_message = http_request()

# Vérifier si le message de succès a été obtenu correctement
if success_message:
    print("Message de succès retourné :", success_message)
else:
    print("La requête n'a pas retourné le message de succès attendu.")



#test task 3
from second_part.src import first_day_of_last_week_in_current_month
formatted_date = first_day_of_last_week_in_current_month()
print(f"Premier jour de la dernière semaine dans le mois en cours: {formatted_date}")

#test task 5
from second_part.src import http_request
import pytest

def test_access_website():
    website = AccessWebsite()
    website.login(username="admin", password="admin")
    assert website.access_website() == "Success"

    website2 = AccessWebsite()
    with pytest.raises(Exception):
        website2.access_website()

    website2.login(username="test", password="test")
    with pytest.raises(Exception):
        website2.access_website()